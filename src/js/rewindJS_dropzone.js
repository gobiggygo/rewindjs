class rewindJSDropzone {
  constructor(definition) {
  }

  render(o) {
    Dropzone.autoDiscover = false;

    let options = $.extend({
      addRemoveLinks: true,
      allowDocuments: true, //allow document files to be uploaded
      allowImages: true, //allow image files to be uploaded
      dictDefaultMessage: 'Drop files here or click to upload',
      error: function (file, response) {
        file.previewElement.classList.add('dz-error');
        let errMsg = response;
        try {
          errMsg = JSON.parse(response)['err'];
        } catch (e) {
        }
        $(file.previewElement).find('.dz-error-message').text(errMsg);
      },
      maxFiles: 1, //how many files can be uploaded?
      maxFilesize: 5,
      onAdd: function (fileName, binId) {
      }, //a callback after a file is added
      onRemove: function (fileName) {
      }, //a callback after a file is removed
      removedfile: function (file) {
        options.onRemove(file.name);
        let _ref;
        if (file.previewElement) {
          if ((_ref = file.previewElement) !== null) {
            _ref.parentNode.removeChild(file.previewElement);
          }
        }
        return this._updateMaxFilesReachedClass();
      },
      selector: '.dropzone', //the element to add the dropzone to
      success: function (file, response) {
        //SML: this is specific to client
        let binID = (JSON.parse(response))['BIN_ID'];
        file.previewElement.classList.add('dz-success');
        options.onAdd(file.name, String(binID));
      }
    }, o);

    let acceptFiles = options.allowImages ? 'image/gif,image/GIF,image/png,image/PNG,image/jpg,image/JPG,image/jpeg,image/JPEG' : '';
    let fileTypes = options.allowImages ? 'gif, png, jpg, jpeg' : '';
    if (options.allowDocuments) {
      acceptFiles += (acceptFiles ? ',' : '') + 'application/pdf,application/PDF,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document';
      fileTypes += (fileTypes ? ', ' : '') + 'pdf, doc, docx';
    }

    options.acceptedFiles = options.acceptedFiles || acceptFiles;
    options.dictFileTooBig = options.dictFileTooBig || 'Maximum size for file attachment is ' + options.maxFilesize + ' MB';
    options.dictInvalidFileType = options.dictInvalidFileType || 'Only following file types are allowed: ' + fileTypes;
    options.dictMaxFilesExceeded = options.dictMaxFilesExceeded || 'Maximum ' + options.maxFiles + ' uploaded files';

    $(options.selector).dropzone(options);
    this.dropzone = $(options.selector).get(0).dropzone;
  }
}
