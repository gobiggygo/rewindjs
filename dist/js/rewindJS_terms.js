'use strict';

/*
Showing a terms and conditions agreement is a common pattern. This helper method encapsulates a common approach

If includeTerms is set to true in your project's package.json coreConfig, then this file will be included.
The method below is a static method of the rewindJS_app (if your app is standalone) or rewindJSApp (if your app is embedded) class.
Use it like this:

rewindJSApp.showTerms({termsText: 'Agree to these terms', disagreedText: 'Why not?', containerSelector: '#my_element'}); //embedded apps

rewindJS_app.showTerms({termsText: 'Agree to these terms', disagreedText: 'Why not?', containerSelector: '#my_element'}); //standalone apps

 */
$.extend(window['rewindJS_app'] || window['rewindJSApp'], {
  showTerms: function showTerms(o) {

    var options = $.extend({
      termsText: 'Content missing: terms text', //The body text of the terms and conditions
      disagreedText: 'Content missing: terms disagreed text', //The body text to show when someone disagrees with the terms and conditions
      agreedCookieName: 'terms_cookie' + Math.random().toString().split('.')[1], //The name of the cookie to store the user's agreement in
      containerSelector: '', //A CSS selector for an element on screen where the terms and conditions will be shown
      onAgreed: function onAgreed(termsWereShown) {//A function called after the user clicks the agree button
        //termsWereShown - true if the terms were shown before agreement. false if the user had previously agreed and the cookie was still there, bypassing the terms
      },
      onDisagreed: function onDisagreed() {//A function called after the user clicks the disagree button
      },
      agreementTitle: 'Terms of Use Agreement' //The title to show at the top
    }, o);

    if (!options.containerSelector) {
      throw new Error('missing container selector for showTerms');
    }

    if ($.cookie(options.agreedCookieName) !== "agree") {
      $(options.containerSelector).html('<section id="rewindJS-terms">' + '<div id="rewindJS-terms-title"><h2 tabindex="-1">' + options.agreementTitle + '</h2></div>' + '<div class="row">' + '<article class="media col-xs-12">' + '<div id="rewindJS-terms-body">' + options.termsText + '</div>' + '<div class="btn-toolbar">' + '<div class="btn-group"><button id="rewindJS-terms-agree" class="btn btn-primary" type="button">Agree</button></div>' + '<div class="btn-group"><button id="rewindJS-terms-disagree" class="btn btn-primary" type="button">Disagree</button></div>' + '</div>' + '</article>' + '</div>' + '</section>');

      $("#rewindJS-terms-agree").click(function () {
        $.cookie(options.agreedCookieName, 'agree');
        $('#rewindJS-terms').remove();
        options.onAgreed(true);
      });

      $("#rewindJS-terms-disagree").click(function () {
        $('#rewindJS-terms').remove();
        $(options.containerSelector).html('<section id="rewindJS-terms">' + '<div id="rewindJS-terms-title"><h2 tabindex="-1">Unable to Proceed</h2></div>' + '<div class="row">' + '<article class="media col-xs-12">' + '<div id="rewindJS-terms-body">' + options.disagreedText + '</div>' + '<div class="buttons">' + '<button id="rewindJS-terms-return" class="btn btn-primary" type="button">Terms and Conditions</button>' + '</div>' + '</article>' + '</div>' + '</section>');
        $("#rewindJS-terms-return").click(function () {
          $('#rewindJS-terms').remove();
          (window['rewindJS_app'] || window['rewindJSApp']).showTerms(options);
          $('#rewindJS-terms-agree').focus();
        }).focus();
        options.onDisagreed();
      });
    } else {
      options.onAgreed(false);
    }
  }
});