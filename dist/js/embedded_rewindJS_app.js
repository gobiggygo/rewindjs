'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var rewindJSApp = function () {
  function rewindJSApp() {
    _classCallCheck(this, rewindJSApp);

    this.appContentKeySuffix = '/';
  }

  _createClass(rewindJSApp, [{
    key: 'loadAppContent',
    value: function loadAppContent(o) {
      var data = {},
          options = $.extend({
        keys: [], //an array of titles of API Content Snippets from WP
        tag: '', //a future option, where you can get multiple snippets with a single tag
        onComplete: function onComplete(data) {//called after all API calls are completed
          //data - a key/value hash with each key name and the associated fetched data value
        },
        onProgress: function onProgress(keyName, errXhr, errMsgOne, errMsgTwo) {//called after a single key is loaded successfully or not successfully
          //keyName - the key that was just finished
          //errXhr, errMsgOne, errMsgTwo - error information if the API call failed
        }
      }, o),
          this1 = this;

      if (options.tag) {
        //TODO: grab all content for a given tag
      } else {
        var count = 0;
        options.keys.forEach(function (key) {
          $.ajax({
            url: '/utility/' + key + this1.appContentKeySuffix,
            success: function success(result) {
              data[key] = result;
              options.onProgress(key);
            },
            error: function error(a, b, c) {
              options.onProgress(key, a, b, c);
            },
            complete: function complete() {
              count++;
              if (count >= options.keys.length) {
                options.onComplete(data);
              }
            }
          });
        });
        if (options.keys.length === 0) {
          options.onComplete(data);
        }
      }
    }
  }]);

  return rewindJSApp;
}();